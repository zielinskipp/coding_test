# Welcome to this "coding test"

## Instructions

Please read this documentation **carefully** and <ins>fix</ins> the issues with the broken code in `main.py` and `seq_tools/seq_braker.py`.

Remember that it is the responsability of the **developer** to keep find errors in documentation as well as code: so keep an eye out for that.