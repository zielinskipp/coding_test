FROM linuxserver/code-server:latest
WORKDIR /home/interview
RUN sudo apt-get update && \
    sudo apt-get -y install python3 python3-pip

RUN pip3 install pandas
RUN python3 -c "import pandas"
