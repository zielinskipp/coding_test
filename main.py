### Fix the code below: ###

import pandas as pd

#######################
# do not modify this ->
#######################

# 3 amino acid sequences a re defined
seq1 = 'YKYRYYITPFLLQEQFYLP'
seq2 = 'CYACWCWRRMKDVRHELKYIWGLIHKSDGRAEQSFQANLHHLKIRLIYTNVDVCNV'
seq3 = 'RPHCPVSAVMYSGGCQCFHSTGRMC'

# a data frame that defines 1-based intervals
my_regions = pd.DataFrame(
    {
        'seq_name'  : ['region1', 'region2', 'region3'],
        'seq_start' : [3, 44, 73],
        'seq_end'   : [25, 69, 88],
        'project'   : ['coding test', 'coding test', 'coding test']
    }
)

#################
# <- up to here 
#################

# fix the code below and comment on your fixes

seq_slices = seq_braker.split_a_sequence(
    seq = [seq1, seq2, seq3],
    tbl = my_regions
    )

# test data
assert seq_slices["region2"] == 'HKSDGRAEQSFQANLHHLKIRLIYTN'
assert list(seq_slices.keys()) == ['region1', 'region2', 'region3']

# print done
print("You passed!")
