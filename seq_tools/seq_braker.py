import pandas as pd

class NotADataFrame(Exception):
    ''' Not a DataFrame exception class '''
    pass

class NotAString(Exception):
    ''' Not a String exception class '''
    pass

aa = 'ACDEFGHIKLMNPQRSTVWY'

def split_a_sequence(seq, tbl):
    '''
    Splits an amino acid sequence into subsequences
    
    :param sequence:   aa string
    :param data_table: a DataFrame with coords
    
    :result list:      a list of aa strings
    '''
    if not isinstance(seq, str):
        raise NotAString('Not a String')
    
    if not isinstance(tbl, pandas.core.frame.DataFrame):
        raise NotADataFrame('Not a Pandas DataFrame')
    
    result = {}
    for i in range(tbl.shape[1]-2):
        result[tbl["seq_name"][i]] = seq[tbl["seq_start"][i]:tbl["seq_end"][i]]
    
    return result
    